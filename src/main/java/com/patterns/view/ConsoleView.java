package com.patterns.view;

import com.patterns.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class ConsoleView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - execute method");

        menu.put("Q", "Q");
    }



    public ConsoleView() {
        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::method);

    }

    private void method() {
        logger.info("Enter task number to change: ");
        Scanner scanner = new Scanner(System.in);
    }

    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
