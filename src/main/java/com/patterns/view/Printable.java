package com.patterns.view;

@FunctionalInterface
public interface Printable {
    void print();
}
